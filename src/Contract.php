<?php
    namespace Zimplify\Starter;
    use Zimplify\Starter\Enterprise;
    use Zimplify\Starter\SecurityException;
    use Zimplify\Core\Application;
    use Zimplify\Core\Document;
    use Zimplify\Core\Event;
    use Zimplify\Core\Module;
    use Zimplify\Core\Services\ClassUtils;
    use \DateInterval;
    use \DateTime;
    use \Exception;
    use \RuntimeException;

    abstract class Contract extends Document {

        const ACL_CLIENT_OPER = "internal.operator";
        const ACL_CLIENT_ACTIONS = "internal.clients";
        const ERR_BAD_ENTERPISE = 4009105002;
        const ERR_BAD_AUTHOR = 4009105003;
        const ERR_NO_CREDIT = 5009105005;
        const ERR_SEC_ACLPARAM = 4019105001;
        const ERR_SEC_BADUSER = 4019105004;
        const FLD_PREPAID = "condition.prebill";

        /**
         * magic __get override for security clearance
         * @param string $param
         * @return mixed
         */
        public function __get(string $param) {
            if (!$this->isUnique()) 
                if (!$this->author()->allow(self::ACL_CLIENT_OPER))
                    throw new SecurityException("Attempt to access unauthorized sections.", self::ERR_SEC_ACLPARAM);
            
            // return to normal flow
            return parent::__get($param);
        }

        /**
         * create alert for the contract instance
         * @param string $event the event that we need to record
         * @param array $args (optional) the parameters are issuing
         * @return Event
         */
        public abstract function alert(string $event, array $args = []) : Event;

        /**
         * the main function to trigger the fulfillment of the contract
         * @return bool
         */
        public abstract function fulfill() : bool;

        /**
         * terminating the fulfillment of this contract
         * @return bool
         */
        public abstract function terminate() : bool;
    }