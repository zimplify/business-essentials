<?php
    namespace Zimplify\Starter;
    use Zimplify\Core\Application;
    use Zimplify\Core\Message;
    use \RumtimeException;

    /**
     * the Email object represents the message to send over to internal AND extermal receipients via SMTP
     * @package Zimplify\Starter (code 9)
     * @type instance (code 1)
     * @file Email (code 12)
     */
    final class Email extends Message {

        /**
         * revert if we are using HTML formatting for the body
         * @return bool
         */
        public function isHtml() : bool {
            return $this->format === "html";
        }

        /**
         * check if the email address is valid
         * @param string $receipient the email address to send to
         * @return bool
         */
        protected function validate(string $receipient) : bool {
            return preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $receipient) ? true : false;
        }
    }