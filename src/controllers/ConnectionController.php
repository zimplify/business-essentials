<?php
    namespace Zimplify\Starter\Controllers;
    use Zimplify\Core\Services\ClassUtils;
    use Zimplify\Core\Application;
    use Zimplify\Core\Controller;
    use Zimplify\Core\ErrorResponse;
    use Zimplify\Core\Reply;
    use Slim\Http\Request;
    use Slim\Http\Response;
    use Firebase\JWT\JWT;    
    use \DateTime;
    use \Exception;    
    use \RuntimeException;

    /**
     * the Controller that manages device connection
     * @package Zimplify\Starter (code 9)
     * @type controller (code 4)
     * @file ConnectionController (code 01)
     */    
    class ConnectionController extends Controller {

        const CFG_KEY_HANDSKS = "application.security.keys.handshakes";
        const CFG_KEY_SESSION = "application.security.keys.session";        
        const CLS_AGENT = "Zimplify\\Core\\Agent";
        const DEVICE_MOBILE = "mobile";
        const ERR_NO_TOKENS = 4009401001;
        const ERR_BAD_TOKEN = 4009401002;
        const ERR_BAD_AGENT = 4019401003;
        const FLD_ADDR = "address";
        const FLD_CONTACT_EMAIL = "contact.email";
        const FLD_DEVICE = "device";
        const FLD_DEV_FLAG = "dev";
        const FLD_EMAIL = "email";
        const FLD_EXPIRY = "expiry";
        const FLD_OBJECT = "object";
        const FLD_SECRET = "secret";
        const FLD_TARGET = "agent";
        const FLD_TOKEN = "token";
        const HDR_DEVICE_TOKEN = "X-Cendol-Device";        
        const HDR_USAGE_TOKEN = "X-Cendol-Token";
        const KEY_HS256_HASH = "HS256";
        const PDR_SECURE_TOKEN = "starter::secure-token";
        const SRF_ID = "id";

        /**
         * creating a new connection on device
         * @param Request $req the incoming request
         * @param Response $res the outgoing response
         * @param array $args the callable array
         * @return Response
         */
        public static function create(Request $req, Response $res, array $args) : Response {
            try {
                $data = $req->getParsedBody();

                // checking our token value
                if (array_key_exists(self::FLD_TOKEN, $data)) {
                    $token = $data[self::FLD_TOKEN];
                    $shakes = Application::env(self::CFG_KEY_HANDSKS);
                    $address = null;
                    $device = null;
                    $agent = null;

                    // let's do the device detection here...
                    foreach ($shakes as $propose => $shake) {
                        try {
                            $cipher = JWT::decode($token, $shake, [self::KEY_HS256_HASH]);

                            if (is_object($cipher)) {
                                // make sure our cipher is friendly
                                if (!property_exists($cipher, self::FLD_ADDR)) 
                                    throw new RuntimeException("Token submitted does not comply with requirmeents", 401);

                                // make sure we mark the device
                                $device = $propose;

                                // recording our address
                                $address = $cipher->{self::FLD_ADDR};
                                break;  
                            } else
                                throw new RuntimeException("Token is invalid", 400);        
                        } catch (Exception $ex) {
                            // we do not need to do anything here...                        
                        }
                    }

                    // make sure we have a device
                    if (!$device)
                        throw new RuntimeException("No matched device.", 403);
                    
                    // now let's check the device needs to be logged in.
                    if ($device != self::DEVICE_MOBILE) {
                        // then we need to do authentication
                        if (property_exists($cipher, self::FLD_TARGET) && property_exists($cipher, self::FLD_SECRET)) {

                            // try to load our agent
                            $agent = Application::search([self::FLD_CONTACT_EMAIL => $cipher->{self::FLD_TARGET}]);

                            // now do the agent validation
                            if (count($agent) > 0 && ClassUtils::is($agent[0], self::CLS_AGENT)) {
                                $agent = $agent[0];
                                $check = !(property_exists($cipher, self::FLD_DEV_FLAG) && $cipher->{self::FLD_DEV_FLAG} === true);
                                if ($check)
                                    if (!$agent->authenticate($cipher->{self::FLD_SECRET}))
                                        throw new SecurityException("Unable to authenticate user ".$data["target"], 401);    
                            } else 
                                throw new RuntimeException("Agent is not available.", 404);
                        } else 
                            throw new RuntimeException("Invalid token for agent type.", 400);
                    }

                    // now let's create our device icon
                    $adapter = Application::request(self::PDR_SECURE_TOKEN, []);
                    $token_d = $adapter->encode([self::FLD_DEVICE => $device, self::FLD_ADDR => $address]);                    
                    
                    // now encode the response
                    $result = new Reply();
                    $headers = [self::HDR_DEVICE_TOKEN => $token_d];

                    // if our agent is valid
                    if ($agent)  {
                        $headers[self::HDR_USAGE_TOKEN] = $agent->generate($device, $address);
                        $result->withStatus(208);
                    }
                    
                    // encode and return
                    $result->withHeader($headers);
                    $result->withJson(null);
                } else 
                    throw new RuntimeException("No token data provided.", 400);
            } catch (Exception $ex) {
                error_log("ERROR: ".$ex->getMessage());
                $result = (new ErrorResponse())->withError($ex);
            } finally {
                return $result->asResponse();
            }

        }

        /**
         * regenerate a new token
         * @param Request $req the incoming request
         * @param Response $res the outgoing response
         * @param array $args the callable array
         * @return Response
         */        
        public static function renew(Request $req, Response $res, array $args) : Response {
            try {
                $old = $req->getHeader(self::HDR_USAGE_TOKEN);
                $device = $req->getHeader(self::HDR_USAGE_TOKEN);

                // token checks
                if (count($old) == 0 && count($device) == 0) 
                    throw new RuntimeException("Renew conditions are not met.", self::ERR_NO_TOKENS);

                // get user token from the old guy
                $token = Application::request(self::PDR_SECURE_TOKEN, [])->decode($old[0]);

                // now inspect the old token
                // if we have a real token
                if (is_array($token)) {
                    
                    // checking the token data for the real values
                    if (array_key_exists(self::FLD_OBJECT, $token) && 
                        array_key_exists(self::FLD_EXPIRY, $token) &&
                        array_key_exists(self::FLD_DEVICE, $token) &&
                        array_key_exists(self::FLD_ADDR, $token)) {

                        // loading the agent
                        $agent = Application::search([self::SRF_ID => $token[self::FLD_OBJECT]]);

                        // validate the agent
                        if (count($agent) > 0) {
                            $agent = $agent[0];
                            $token = $agent->generate($token[self::FLD_DEVICE], $token[self::FLD_ADDR]);

                            $result = new Reply();
                            $result->withStatus(208)->withHeader([
                                self::HDR_DEVICE_TOKEN => $device[0],
                                self::HDR_USAGE_TOKEN => $token
                            ]);
                        } else 
                            throw new RuntimeException("Inappropriate agent for request.", self::ERR_BAD_AGENT);       
                    } else 
                        throw new RuntimeException("Token is does not carry the correct data fields", self::ERR_BAD_TOKEN);
                } else 
                    throw new RuntimeException("Token is not correct", self::ERR_BAD_TOKEN);

            } catch (Exception $ex) {
                error_log("ERROR: ".$ex->getMessage());
                $result = (new ErrorResponse())->withError($ex);
            } finally {
                return $result->asResponse();
            }
        }

        /**
         * to reclaim a token key if user already exist but reinstalled (no pass key)
         * TODO: need to tighten the device...
         * @param Request $req the incoming request
         * @param Response $res the outgoing response
         * @param array $args the callable array
         * @return Response
         */            
        public static function reclaim(Request $req, Response $res, array $args) : Response {
            try {
                $devices = $req->getHeader(self::HDR_DEVICE_TOKEN);
                if (count($devices) > 0) {
                    // need to disect our device token:
                    $adapter = Application::request(self::PDR_SECURE_TOKEN, []);
                    $device = $adapter->decode($devices[0]);

                    // now dealing with the body
                    $body = $req->getParsedBody();
                    if (array_key_exists(self::FLD_EMAIL, $body)) {
                        $agent = Application::search([self::FLD_CONTACT_EMAIL => $body[self::FLD_EMAIL]]);
                        
                        if (count($agent) == 0) 
                            throw new RuntimeException("Unable to complete request", 404);
                        else {
                            $token = $agent[0]->generate($device[self::FLD_DEVICE], $device[self::FLD_ADDR]);
    
                            // now encode our response
                            $headers = [self::HDR_DEVICE_TOKEN => $devices[0], self::HDR_USAGE_TOKEN => $token];

                            // setting the reply
                            $result = new Reply();
                            $result->withStatus(208)->withHeader($headers)->withJson(null);
                        }
                    } else  
                        throw new RuntimeException("Invalid request body", 400);
                } else  
                    throw new RuntimeException("Expect device token but is not presented.", 400);
            } catch (Exception $ex) {
                error_log("ERROR: ".$ex->getMessage());
                $result = (new ErrorResponse())->withError($ex);                
            } finally {
                return $result->asResponse();
            }
        }
    }