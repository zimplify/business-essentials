<?php
    namespace Zimplify\Starter\Controllers;
    use Zimplify\Core\Agent;
    use Zimplify\Core\Application;
    use Zimplify\Core\Controller;
    use Zimplify\Core\ErrorResponse;
    use Zimplify\Core\Reply;
    use Slim\Http\Request;
    use Slim\Http\Response;
    use \DateTime;
    use \Exception;

    /**
     * the Controller that manages device connection
     * @package Zimplify\Starter (code 9)
     * @type controller (code 4)
     * @file ConnectionController (code 03)
     */        
    class NotificationsController extends Controller {

        const ATTR_AGENT = "agent";
        const CLS_NOTIFICATION = "starter::notification";
        const DEF_EVERYONE = "all";
        const SRC_BIND = "bind";
        const SRC_MATCH = "match";
        const SRC_OPER = "oper";
        const SRC_VALUE = "value";
        const SRF_RECEIPIENTS = "receipients";
        const SRF_TYPE = "type";

        /**
         * generating new notifications for all agents on system
         * @param Request $req the incoming request
         * @param Response $res the outgoing response
         * @param array $args the URL part variables
         * @return Response
         */
        public static function broadcast(Request $req, Response $res, array $args = []) : Response {
            try {
                $agent = $req->getAttribute(self::ATTR_AGENT);
                
                // generating our message
                $message = Application::create(self::CLS_NOTIFICATION, $agent->parent() ?? $agent);
                $body = $req->getParsedBody();
                $body["expiry"] = DateTime::createFromFormat("U", $body["expiry"]);

                // we need to filter to what form of broadcast we are talking
                if (array_key_exists("receipients", $body)) {
                    $targets = $body["receipients"];
                    if (substr_count($targets,"all") == 1) {
                        if ($targets !== "all") {
                            $targets = explode("::", $targets);
                            if (count($targets) == 2) {
                                switch ($targets[1]) {
                                    case "staff":
                                    case "representatives":
                                        break;
                                    default:
                                        throw new RuntimeException("Unknown target ".$targets[1]." used for broadcast", 404);
                                }
                            } else
                                throw new RuntimeException("Improper target for broadcasting.", 400);
                        }                        
                    } else 
                        $body["receipients"] = ["all"];    
                } else 
                    $body["receipients"] = ["all"];

                // now parsing in
                $message->populate($body);
                $message->save();

                // now we have to send things back
                $data = static::display($message, $agent);

                // generating reply
                $result = new Reply();
                $result->withStatus(202)->withJson($data);

            } catch (Exception $ex) {
                error_log("Error: ".$ex->getMessage());
                $result = (new ErrorResponse())->withError($ex);
            } finally {
                return $result->asResponse();
            }
        }

        /**
         * generating new notifications for agents
         * @param Request $req the incoming request
         * @param Response $res the outgoing response
         * @param array $args the URL part variables
         * @return Response
         */
        public static function create(Request $req, Response $res, array $args = []) : Response {
            try {
                $agent = $req->getAttribute(self::ATTR_AGENT);
                
                // generating our message
                $message = Application::create(self::CLS_NOTIFICATION, $agent->parent() ?? $agent);
                $body = $req->getParsedBody();
                $body["expiry"] = DateTime::createFromFormat("U", $body["expiry"]);
                $body["receipients"] = explode(",", $body["receipients"]);

                // now parsing in
                $message->populate($body);
                $message->save();

                // now we have to send things back
                $data = static::display($message, $agent);

                // generating reply
                $result = new Reply();
                $result->withStatus(202)->withJson($data);

            } catch (Exception $ex) {
                error_log("Error: ".$ex->getMessage());
                $result = (new ErrorResponse())->withError($ex);
            } finally {
                return $result->asResponse();
            }
        }

        /**
         * generating new notifications for agents
         * @param Request $req the incoming request
         * @param Response $res the outgoing response
         * @param array $args the URL part variables
         * @return Response
         */        
        public static function fetch(Request $req, Response $res, array $args = []) : Response {
            try {
                $agent = $req->getAttribute(self::ATTR_AGENT);
                
                // now we need to see if there are things for me
                $messages = Application::search([
                    self::SRF_TYPE => self::CLS_NOTIFICATION, 
                    [self::SRC_MATCH => self::SRF_RECEIPIENTS, self::SRC_BIND => "or", self::SRC_VALUE => $agent->id, self::SRC_OPER => "ct"],
                    [self::SRC_MATCH => self::SRF_RECEIPIENTS, self::SRC_BIND => "or", self::SRC_VALUE => self::DEF_EVERYONE]
                ], "messages");

                // now we build the result data
                $data = [];
                foreach (array_reverse($messages) as $m) 
                    array_push($data, static::display($m, $agent));
                
                // now getting our result
                $result = new Reply();
                $result->withJson($data);
            } catch (Exception $ex) {
                error_log("Error :".$ex->getMessage());
                $result = (new ErrorResponse())->withError($ex);
            } finally {
                return $result->asResponse();
            }
        }
    }