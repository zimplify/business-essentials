<?php
    namespace Zimplify\Starter;
    use Zimplify\Core\Agent;
    use Zimplify\Core\Application;
    use Zimplify\Core\Controller;
    use Zimplify\Core\ErrorResponse;
    use Zimplify\Core\Reply;
    use Zimplify\Starter\Inbox;
    use Zimplify\Starter\Ping;    
    use Slim\Http\Request;
    use Slim\Http\Response;
    use \DateTime;
    use \DateInterval;
    use \Exception;
    use \RuntimeException;

    /**
     * the Controller that monitors the inbox activities
     * @package Zimplify\Starter (code 9)
     * @type controller (code 4)
     * @file AgentInboxController (code 02)
     */
    class PingController extends Controller {

        const ATTR_AGENT = "agent";
        const ATTR_DRIVER = "driver";
        const ATTR_STAFF = "staff";
        const CLS_PING = "starter::ping";
        const FLD_MESSAGE = "message";
        const FLD_RECEIPIENTS = "receipients";

        /**
         * clearing the messages in an inbox
         * @param Request $req the incoming request
         * @param Response $res the outgoing response
         * @param array $args the parameter array for URL
         * @return Response
         */
        public static function clear(Request $req, Response $res, array $args) : Response {
            try {
                $staff = $req->getAttribute(self::ATTR_AGENT);
                $inbox = new Inbox($staff);
                $inbox->clear();
                $result = new Reply();
                $result->withStatus(202);
            } catch (Exception $ex) {
                error_log("ERROR: ".$ex->getMessage());
                $result = (new ErrorResponse())->withError($ex);
            } finally {
                return $result->asResponse();
            }
        }

        /**
         * creating a new ping to send into instance
         * @param Request $req the incoming request
         * @param Response $res the outgoing response
         * @param array $args the URL parameters
         * @return Response
         */
        public static function create(Request $req, Response $res, array $args) : Response {
            try {

                // loading our agent
                $agent = $req->getAttribute(self::ATTR_STAFF);
                $body = $req->getParsedBody();

                // make sure the receipients are legit
                if (array_key_exists(self::FLD_RECEIPIENTS, $body) && $agent->isNetwork($body[self::FLD_RECEIPIENTS])) {

                    // getting the message body data
                    $message = Application::create(self::CLS_PING, $agent);
                    $message->populate($body);
                    $message->save();

                    // save and return the message
                    $result = new Reply();
                    $result->withJson(202)->withJson(null);

                } else 
                    throw new RuntimeException("Unknown receipients or outside network.", 404);

            } catch (Exception $ex) {
                error_log("ERROR: ".$ex->getMessage());
                $result = (new ErrorResponse())->withError($ex);
            } finally {
                return $result->asResponse();
            }
        }
        
        /**
         * deleting a message from the inbox
         * @param Request $req the incoming request
         * @param Response $res the outgoing response
         * @param array $args the array of params for URL
         * @return Response
         */
        public static function delete(Request $req, Response $res, array $args) : Resposne {
            try {
                $staff = $req->getAttribute(self::ATTR_AGENT);
                $body = $req->getParsedBody();
                if (array_key_exists(self::FLD_MESSAGE, $body)) {
                    $message = Application::load($body[self::FLD_MESSAGE], self::CLS_PING);
                    $message->delete();
                    $result = new Reply();
                    $result->withStatus(202)->withJson(null);
                } else 
                    throw new RuntimeException("Unable to locate message.", 404);
                    
            } catch (Exception $ex) {
                error_log("ERROR: ".$ex->getMessage());
                $result = (new ErrorRepsonse())->withError($ex);
            } finally {
                return $result->asResponse();
            }
        }

        /**
         * generating the result dataset from the original data
         * @param Instance $source the source dataset
         * @param Agent $user the user to evaluate exclusions
         * @return array 
         */        
        protected static function display(Instance $source, Agent $user = null) : array {
            $result = parent::display($source, $user);

            // adding or charging data                        
            $result["created"] = $result["created"] * 1000;

            // adding sender data
            $sender = [];
            $result["sender"] = $sender;

            // removing data
            unset($result["update"]);

            // returning data
            return $result;
        }

        /**
         * reading out what is in the inbox
         * @param Request $req the incoming request
         * @param Response $res the outgoing response
         * @param array $args the array of URL parameters
         * @return Response
         */
        public static function read(Request $req, Response $res, array $args) : Response {
            try {
                $staff = $req->getAttribute(self::ATTR_STAFF);
                $inbox = new Inbox($staff);
                $data = [];
                foreach ($inbox->received  as $m) 
                    array_push($data, static::display($m, $staff));
                $result = new Reply();
                $result->withJson($data);
            } catch (Exception $ex) {
                error_log("ERROR: ".$ex->getMessage());                
                $result = (new ErrorResponse())->withError($ex);
            } finally {
                return $result->asResponse();
            }
        }

        /**
         * collecting all sent messages
         * @param Request $req the incoming request
         * @param Response $res the outgoing response
         * @param array $args the parameter array in URL
         * @return Response
         */
        public static function sent(Request $req, Response $res, array $args) : Response {
            try {
                $staff = $req->getAttribute(self::ATTR_AGENT);
                $inbox = new Inbox($staff);
                $data = [];
                foreach ($inbox->sent as $m) 
                    array_push($data, static::display($m, $staff));
                $result = new Reply();
                $result->withJson($data);
            } catch (Exception $ex) {
                error_log("ERROR: ".$ex->getMessage());
                $result =  (new ErrorResponse())->wtihError($ex);
            } finally {
                return $result->asResponse();
            }
        }

        /**
         * sending a reply on an existing message
         * @param Request $req the incoming request
         * @param Response $res the outgoing response
         * @param array $args the parameers in the URL
         * @return Response
         */
        public static function update(Request $req, Response $res, array $args) : Response {
            try {

                // make sure we have a message
                if (array_key_exists(self::PARAM_MESSAGE, $args)) {

                    // collecting our agent
                    $agent = $req->getAttribute(self::ATTR_AGENT);
                    $body = $req->getParsedBody();

                    // now loading the original message
                    $message = Application::load($args[self::PARAM_MESSAGE], self::CLS_PING);
                    $reply = $message->reply($body);

                    // establish response
                    $result = new Reply();
                    $result->withStatus(202)->withJson(static::display($reply, $agent));

                } else 
                    throw new RuntimeException("Unknown message to append reply.", 404);

            } catch (Exception $ex) {
                error_log("ERROR: ".$ex->getMessage());
                $result = (new ErrorResponse())->withError($ex);
            } finally {
                return $result->asResponse();
            }
        }

    }