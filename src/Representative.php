<?php
    namespace Zimplify\Starter;
    use Zimplify\Core\Application;
    use Zimplify\Core\Agent;
    use \DateInterval;
    use \DateTime;
    use \Exception;
    use \RuntimeException;

    /**
     * the Representative object is the Enterprise staff accessing the system
     * @package Zimplify\Starter (code 9)
     * @type instance (code 1)
     * @file Representative (code 04)
     */
    class Representative extends Agent {

        const HDF_OBJECT = "object";
        const HDF_EXPIRY = "expiry";
        const HDF_DEVICE = "device";
        const HDF_ADDRESS = "address";
        const PDR_SECURE_TOKEN = "starter::secure-token";

        /**
         * generating a new token for headers
         * @param string $device the device type when accessing
         * @param string $client the source address
         * @return string
         */
        public function generate(string $device, string $client) : string {
            $expiry = (new DateTime())->add(new DateInterval("P7D"));
            $data = [self::HDF_OBJECT => $this->id, self::HDF_DEVICE => $device, self::HDF_ADDRESS => $client, self::HDF_EXPIRY => $expiry->format("U")];
            return Application::request(self::PDR_SECURE_TOKEN, [])->encode($data);
        }

        /**
         * handling the actions we need to do before deleting the account.
         * @return void
         */
        protected function revoke() {
            // not much we have to do here
        }        
    }