<?php
    namespace Zimplify\Starter;
    use Zimplify\Core\Application;
    use Zimplify\Core\Document;
    use Zimplify\Core\File;
    use \DateInterval;
    use \DateTime;
    use \Exception;
    use \RuntimeException;

    /**
     * the Invoice is the base for chargability for Zimplify Application
     * @package Zimplify\Starter (code 9)
     * @type Document (code 2)
     * @file Invoice (code 08)
     */
    abstract class Invoice extends Document {

        /**
         * calculate the tax amount applicable to this invoice
         * @param float $tax the tax rate to apply for this invoice.
         * @return float
         */
        protected abstract function calculation(float $tax) : float;

        /**
         * request to collect the amount from this invoice
         * @param float $amount (optional) the ADDITIONAL amount charged to the invoiced amount if applicable
         * @return string the transaction receipt number from the transaction
         */
        public abstract function charge(float $amount = 0) : string;

        /**
         * request to generate the invoice in PDF version and ready to send back as a PDF format
         * @return File
         */
        public abstract function render() : File;

        /**
         * generate the document serial number for this type of invoice.
         * @return string
         */
        public abstract function serialize() : string;

    }