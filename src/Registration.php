<?php
    namespace Zimplify\Starter;
    use Zimplify\Starter\Enterprise;
    use Zimplify\Core\Application;
    use Zimplify\Core\Document;
    use Zimplify\Core\Module;
    use \DateInterval;
    use \DateTime;
    use \Exception;
    use \RuntimeException;
    
    /**
     * the Registration document is to allow us to build a scaffold for Enterprise onboarding
     * @package Zimplify\Starter (code 9)
     * @type document (code 2)
     * @file Registration (code 07)
     */
    abstract class Registration extends Document {

        /**
         * the main routine to generate the enterprise instnace
         * @return Enterprise
         */
        public abstract function register() : Enterprise;
        
    }