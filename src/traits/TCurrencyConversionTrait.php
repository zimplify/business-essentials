<?php
    namespace Zimplify\Starter\Traits;

    /**
     * @package Zimplify\Starte (code 9)
     * @type trait (code 8)
     * @file TCurrencyConversionTrait (code 01)
     */
    trait TCurrencyConversionTrait {

        /**
         * get the price of a product from specific currency
         * @param string $currency the currency code
         * @return float
         */
        public function convert(string $from, string $to, float $amount) {
            $result = Application::request(self::PDR_CURRENCY_CONVERT, [])
                ->convert($from, $to, $amount);
            return $result;
        }        
    }