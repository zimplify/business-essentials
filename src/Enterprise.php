<?php
    namespace Zimplify\Starter;
    use Zimplify\Core\Application;
    use Zimplify\Core\Event;    
    use Zimplify\Core\Instance;
    use Zimplify\Core\Module;
    use \DateInterval;
    use \DateTime;
    use \Exception;

    /**
     * the Enterprise forms the entities that has BUSINESS TRANSACTIONS over Zimplify application
     * @package Zimplify\Starter (code 9)
     * @type instance (code 1)
     * @file Enterprise (code 02)
     */
    abstract class Enterprise extends Instance {

        const CFG_MAX_REPS = "max_representatives";
        const CLS_REPRESENTATIVE = "starter::representative";
        const DEF_ACCOUNT_MANAGER = "manager";
        const DEF_CONTRACTS = "contracts";
        const DEF_HISTORY = "histories";
        const DEF_INVOICES = "invoices";
        const DEF_OVERDUE = "overdue";
        const DEF_OUTSTANDING = "outstanding";
        const DEF_REPRESENTATIVES = "staff";
        const ERR_MAX_REPS = 5009102001;
        const FLD_EMAIL = "contact.email";
        const FLD_MOBILE = "contact.mobile";
        const FLD_NAME_FIRST = "contact.name.first";
        const FLD_NAME_LAST = "contact.name.last";
        const FLD_ROLES = "roles";
        const SRF_ID = "id";

        /**
         * the magic get override method
         * @param string $param the field to read
         * @return mixed
         */
        public function __get(string $param) {
            $result = null;
            switch ($param) {
                case self::DEF_ACCOUNT_MANAGER: 
                    if ($this->liaison) {
                        $result = Application::search([self::SRF_ID => $this->liaison, Instance::FLD_STATUS => true]);
                        $result = count($result) > 0 ? $result[0] : null;
                    } 
                    break;
                case self::DEF_CONTRACTS: break;
                case self::DEF_HISTORY: Application::search([self::SRF_ID => $this->id], "events"); break;
                case self::DEF_INVOICES: 
                    $result = Application::search([Instance::DS_TYPE => self::CLS_INVOICE, Instance::FLD_PARENT_ID => $this->id], "documents");
                    break;
                case self::DEF_OUTSTANDING:
                    $invoices = Application::search([Instance::DS_TYPE => self::CLS_INVOICE, Instance::FLD_PARENT_ID => $this->id, Instance::FLD_STATUS => true], "documents");
                    if (count($invoices) > 0) {
                        $result = 0;
                        foreach ($invoices as $i) 
                            $result += $i->total;
                    } else 
                        $result = 0;
                    break;
                case self::DEF_OVERDUE: 
                    $invoices = Application::search([Instance::DS_TYPE => self::CLS_INVOICE, Instance::FLD_PARENT_ID => $this->id, Instance::FLD_STATUS => true], "documents");
                    if (count($invoices) > 0) {
                        $result = 0;
                        foreach ($invoices as $i) 
                            if ((new DateTime())->diff($i->due)->invert = 1) 
                                $result += $i->total;                    
                    } else 
                        $result = 0;                
                    break;
                case self::DEF_REPRESENTATIVES: 
                    Application::search([Instance::DS_TYPE => self::CLS_REPRESENTATIVE, Instance::FLD_PARENT_ID => $this->id]); 
                    break;
                default: $result = parent::__get($param);
            }
            return $result;
        }
        
        /**
         * issue a new event for this enterprise
         * @param string $event the event type to cater for
         * @param array $params the parameters to add to the event
         * @return Event
         */
        public abstract function alert(string $event, array $params = []) : Event;

        /**
         * removing a representative to the enteprrise
         * @param string $email the email of the rep to remove
         * @return bool
         */
        public function delist(string $email) : bool {
            $target = Application::search([self::FLD_EMAIL => $email, Instance::FLD_PARENT_ID => $this->id]);
            if (count($target) > 0) {
                return $target[0]->delete();
            } else 
                return false;
        } 

        /**
         * issue a new Representative for the Enterprise
         * @param string $first the first name
         * @param string $last the last name
         * @param string $email the email address of the rep
         * @param string $mobile the mobile number for the rep
         * @param array $roles (optional) the roles to input, if none default is assumed
         * @return Representative
         */
        public function enlist(string $first, string $last, string $email, string $mobile, array $roles = null) : Representative {
            if (count($this->representatives) < (int) Module::env(__NAMESPACE__, self::CFG_MAX_REPS)) {
                $result = Application::create(self::CLS_REPRESENTATIVE, $this);
                $result->{self::FLD_NAME_FIRST} = $first;
                $result->{self::FLD_NAME_LAST} = $first;
                $result->{self::FLD_EMAIL} = $first;
                $result->{self::FLD_MOBILE} = $first;
                if ($role && count($role) > 0) 
                    $result->{self::FLD_ROLES} = implode(",", $roles);
                return $result->save();
            } else 
                throw new \RuntimeException("Max allowed representatives reached.", self::ERR_MAX_REPS);
            
        }

        /**
         * assigning the liaison officer to the enterprise
         * @param Agent $liaison the officer to be responsible for this enterprise
         * @return Enterprise
         */
        public function assign(Agent $liaison) : self {
            $this->{self::FLD_LIAISON_OFFICER} = $liaison->id;
            return $this;
        }
       
        /**
         * issue a charge notice for the enterprise client
         * @param Contract $order the contract that we are charging for
         * @return Invoice
         */
        public function charge(Contract $order) : Invoice {
        }

        /**
         * check if the enterprise can issue a new contract
         * @return bool
         */
        public function isAvailable() : bool {
            return $this->status && !$this->locked;
        }

        /**
         * issue a lockdown on the enterprise
         * @return Enterprise
         */
        public function lock() : self {
            $this->locked = true;
            return $this;
        }

        /**
         * the preparation steps of the instance during initialization
         * @return void
         */        
        protected function prepare() {            
        }        

        /**
         * record the event into the enterprise history log
         * @param string $event the event type
         * @param array $params the parameters to supply to the events
         * @return Enterprise
         */
        public function record(string $event, array $params) : self {
            $event = $this->alert($event, $params)->save();
            return $this;
        }

        /**
         * unlock the enterprise if is already on lockdown
         * @return Enterprise
         */
        public function unlock() : self {
            if ($this->locked) 
                $this->locked = false;
            return $this;
        }

    }