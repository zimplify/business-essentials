<?php
    namespace Zimplify\Starter;
    use Zimplify\Core\Agent;
    use Zimplify\Core\Application;
    use \DateTime;
    use \DateInterval;
    use \RuntimeException;
    
    /**
     * the Inbox object allow an agent (staff or presentative) to get their pings
     * @package Zimplify\Starter (code 9)
     * @type instance (code 1)
     * @file Inbox (code 11)
     */
    final class Inbox {

        const FLD_RECEIVING = "received";
        const FLD_SENDING = "sent";

        private $owner;

        /**
         * initializing our inbox
         * @param Agent the agent of the inbox
         * @return void
         */
        function __construct(Agent $owner) {
            $this->owner = $owner;
        }

        /**
         * the magic method to get things done
         * @param string $parma the param to read
         * @return mixed
         */
        function __get(string $param) {
            $result = null;
            switch ($param) {
                case self::FLD_RCEIVING:
                    $messages = Application::search([
                        self::SRF_TYPE => self::CLS_PING, 
                        ["match" => self::SRF_RECEIVER, "value" => $this->owner->id, "oper" => "ct"]]);
                    $result = $messages;
                    break;
                case self::FLD_SENDING:
                    $messages = Application::search([
                        self::SRF_TYPE => self::CLS_PING, 
                        self::SRF_ONWER => $this->owner->id
                    ]);
                    $result = $messages;
                    break;
            }
            return $result;
        }

        /**
         * clearing the inbox from all messages
         * @return void
         */
        public function clear() {
            foreach ($this->received as $m) 
                $m->delete();
        }
    }