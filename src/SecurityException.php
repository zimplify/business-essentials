<?php
    namespace Zimplify\Starter;
    use \Exception;

    /**
     * the Security Exception is use to ensure the security incidents are logged
     * @package Zimplify\Starter (code 9)
     * @type exception (code 6)
     * @file SecurityException
     */
    class SecurityException extends Exception {
    }