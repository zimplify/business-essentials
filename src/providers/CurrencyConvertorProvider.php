<?php
    namespace Zimplify\Starter\Providers;
    use Zimplify\Core\Provider;
    use Unirest\Request;
    use \RuntimeException;

    /**
     * The provider offer currency exchange rates
     * @package Cendol\CSB (code 2)
     * @instance Provider (code 3)
     * @type CurrencyConvertorProvider (code: 02)
     */    
    class CurrencyConvertorProvider extends Provider {

        const ERR_BAD_FORMAT = 4002302001;
        const URL_API_PATH = "https://api.exchangerate-api.com/v4/latest/";

        /**
         * converting the data. note the currency will get truncate to 3 characters is not specific properly
         * @param string $from currency currently is
         * @param string $to currecy to be
         * @param float $amount the amount to convert
         * @return float
         */
        public function convert(string $from, string $to, float $amount) : float {
            if (strlen($from) > 3 || strlen($to) > 3)
                throw new RuntimeException("Currency is in wrong format.", self::ERR_BAD_FORMAT); 

            // getting our data
            $response = Request::get(self::URL_API_PATH.$from)->body;

            // now getting our data
            $result = round($amount * $response->rates->{$to}, 4);

            // now return
            return $result;
        }

        /**
         * startup initializer for the service
         * @return void
         */
        protected function initialize() {}

        /**
         * check if all startup arguments are available
         * @return bool
         */
        protected function isRequired() : bool { 
            return true; 
        }
                
    }