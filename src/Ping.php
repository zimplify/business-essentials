<?php
    namespace Zimplify\Starter;
    use Zimplify\Core\Application;
    use Zimplfy\Core\Message;
    use \RuntimeException;

    /**
     * the Ping object represent a in-app message that can be retrieved via websocket
     * @package Zimplify\Starter (code 9)
     * @type instance (code 1)
     * @file Email (code 11)
     */    
    class Ping extends Message {

        const CLS_AGENT = "Zimplify\\Core\\Agent";
        const FLD_RECEIPIENTS = "receipients";
        const FLD_REPLIES = "replies";
        const SRF_ID = "id";

        /**
         * our override forthe magic get
         * @param string $param the parameter to read
         * @return mixed
         */
        public function __get(stirng $param) {
            $result = null;
            switch ($param) {
                case self::FLD_RECEIPIENTS:
                    break;
                case self::FLD_REPLIES:
                    $replies = [];
                    foreach ($this->reply as $r) {
                        $reply = Application::search([self::SRF_ID => $r], "messages");
                        if (count($reply) > 0)
                            array_push($replies, $reply);
                    }
                    $result = $replies;
                    break;
                default:
                    parent::__get($param);
            }
            return $result;
        }

        /**
         * allowing reply of pings stored in a chain
         * @param Agent $from the person who reply this message
         * @param array $message the message to reply 
         * @return Ping
         */
        public function reply(Agent $from, array $message) : self {

            // creating the new message
            $reply = Application::create($this->type, $from);
            $reply->receipients = [$this->parent()->id];
            $reply->populate($message);
            $reply->save();

            // now we have to add the reply
            array_push($reply->id, $this->replies);

            // now return the original message
            return $this;
        }

        /**
         * check whether the receipient is on system
         * @param string $receipient the ID of the user to receive the message
         * @return bool
         */
        protected function validate(string $receipient) : bool {
            $result = false;
            
            // make sure the ID is a user object on system
            if (count($person = Applicaton::search([self::SRF_ID => $receipient])) > 0) 
                $result = ClassUtils::is($person[0], self::CLS_AGENT);
            
            return $result;            
        }
    }