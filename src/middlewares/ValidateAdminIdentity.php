<?php
    namespace Zimplify\Starter\Middlewares;
    use Zimplify\Core\Agent;
    use Zimplify\Core\Middlewares\ValidateAgentIdentity;
    use Zimplify\Core\Services\ClassUtils;

    /**
     * this middleware check against the staff or admin agent
     * @package Zimplify\Starter (code 9)
     * @type middleware (code 4)
     * @file ValidateStaffIdentity (code 02)
     */
    class ValidateStaffIdentity extends ValidateAgentIdentity {

        const CLS_ADMIN = "Zimplify\\Starter\\Administrator";        

        /**
         * way to identify our agent is of specific purpose so can issue filter if necessary
         * @param Agent $agent the agent instance to evaluate
         * @return bool
         */
        protected function enabled(Agent $agent) : bool {
            return ClassUtils::is($agent, self::CLS_ADMIN);
        }        
    }