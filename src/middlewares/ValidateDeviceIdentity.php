<?php
    namespace Zimplify\Starter\Middlewares;
    use Zimplifu\Core\Agent;
    use Zimplify\Core\Application;
    use Slim\Http\Request;
    use Slim\Http\Response;
    use \RuntimeException;

    /**
     * handing the device token and identity the type of device and authentication if needed.
     * @package Zimplify\Starter
     * @type middleware (code 4)
     * @file ValidateDeviceIdentity (code 01)
     */
    class ValidateDeviceIdentity {

        const FLD_DEVICE = "device";
        const FLD_ADDR = "address";
        const HDR_DEVICE_TOKEN = "X-Cendol-Device";
        const PDR_SECURE_TOKEN = "starter::secure-token";

        /**
         * our main invoke function
         * @param Request $req the main incoming request
         * @param Response $res the outgoing response 
         * @param callable $next the follow on execution pointer
         * @return Response
         */
        public function __invoke(Request $req, Response $res, callable $next) : Response {

            // now we need to get the request header
            $h = $req->getHeader(self::HDR_DEVICE_TOKEN);

            // loading out the real values
            $v = Application::request(self::PDR_SECURE_TOKEN, [])->decode($h[0]);

            // make sure we can decode the data
            if (is_array($v)) {

                // now to the client reference check...

                // encoding into request
                $req = $req->withAttribute(self::FLD_DEVICE, $v[self::FLD_DEVICE]);
                $req = $req->withAttribute(self::FLD_ADDR, $v[self::FLD_ADDR]);

                // now do routine
                $res = $next($req, $res);
                $res = $res->withHeader(self::HDR_DEVICE_TOKEN, $h[0]);
                return $res;
            } else
                return (new ErrorResponse())->withError(new RuntimeException("Invalid device token.", 401))->asResponse();
        }
    }