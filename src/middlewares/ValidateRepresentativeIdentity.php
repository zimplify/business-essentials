<?php
    namespace Zimplify\Starter\Middlewares;
    use Zimplify\Core\Agent;
    use Zimplify\Core\Middlewares\ValidateAgentIdentity;

    /**
     * this middleware check against the representative
     * @package Zimplify\Starter (code 9)
     * @type middleware (code 4)
     * @file ValidateRepresentativeIdentity (code 04)
     */    
    class ValidateRepresentativeIdentity extends ValidateAgentIdentity {

        const CLS_REP = "Zimplify\\Starter\\Representative";        

        /**
         * way to identify our agent is of specific purpose so can issue filter if necessary
         * @param Agent $agent the agent instance to evaluate
         * @return bool
         */
        protected function enabled(Agent $agent) : bool {
            return ClassUtils::is($agent, self::CLS_REP);
        }                
    }