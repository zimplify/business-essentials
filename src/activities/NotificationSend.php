<?php
    namespace Zimplify\Starter\Activities;
    use Zimplify\Starter\Activities\MessageSend;
    use Zimplify\Starter\Notification;
    use Zimplify\Core\Agent;
    use Zimplify\Core\Instance;    
    use Zimplify\Core\Message;
    use Zimplify\Core\Task;
    use Zimplify\Core\Application;
    use Zimplify\Core\Services\DataUtils;
    use \RuntimeException;

    /**
     * sending out the notification to register agent
     * @package Zimplify\Starter (code 9)
     * @type activity (code 5)
     * @file NotificationSend (code 02)
     */
    class NotificationSend extends MessageSend {
        
        const DEF_ALL_USERS = "*";

        /**
         * getting the people we need to secure in order to send out the message
         * @param Instance $source the data source for us to extract the target data
         * @param array $inputs the supplementary data array
         * @return array
         */
        protected function identify(Instance $source, array $inputs = []) : array {        
            $result = [];
            if ($this->target === self::DEF_ALL_USERS) 
                array_push($result, self::DEF_ALL_USERS);
            else {
                $receivers = DataUtils::evaluate($this->target, $source, $inputs);
                $result = explode(",", $receivers);
            }
            return $result;
        }

        /**
         * generating the message that we are sending
         * @param Agent $sender the sender of the message
         * @param mixed $source the source for us to render
         * @return Message
         */
        protected function prepare(Agent $sender, $source) : Message {
            $result = new Notiification($sender);
            $result = $result->render($this->template, $source);
            return $result;
        }
        
        /**
         * sending out the message to outside
         * @param Message $message the message to send
         * @return bool
         */
        protected function send(Message $message) : bool {
            return $message->save() ? true : false;
        }
    }
