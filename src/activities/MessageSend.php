<?php
    namespace Zimplify\Starter\Activities;
    use Zimplify\Core\Agent;
    use Zimplify\Core\Application;
    use Zimplify\Core\Document;
    use Zimplify\Core\Instance;    
    use Zimplify\Core\Message;
    use Zimplify\Core\Task;

    /**
     * this activity is to provide the infrasructure to send out messages
     * @package Zimplify\Starter (code 9)
     * @type provider (code 3)
     * @file EmailSend (code 1)
     */
    abstract class MessageSend extends Task {

        const FLD_OUTPUT = "output";

        /**
         * getting the people we need to secure in order to send out the message
         * @param Instance $source the data source for us to extract the target data
         * @param array $inputs the supplementary data array
         * @return array
         */
        protected abstract function identify(Instance $source, array $inputs = []) : array;

        /**
         * check if the function is ready to begin.
         * @return bool
         */
        protected function isRequired() : bool {
            return $this->target;
            return $this->template;
        }

        /**
         * generating the message that we are sending
         * @param Agent $sender the sender of the message
         * @param mixed $source the data source us to extract data
         * @return Message
         */
        protected abstract function prepare(Agent $sender, $source) : Message;

        /**
         * running the function internally
         * @param Document $source (referenced) the source to run
         * @param array $inputs (referenced) the step of the functio
         * @return mixed
         */
        protected function run(Document &$source, string &$status = null, array &$inputs = []) {

            // the basic setup we all need
            $sender = $source->author();
            $receipients = $this->identify($source, $inputs) ?? [];
            $result = 0;
            
            // now we need to send out to each receipient
            if (!$this->isDebug()) 
                foreach ($receipients as $receiver) {                    
                        // first let the core of the nessage
                        $message = $this->prepare($sender);
                        $message->receipient = $receiver;

                        // now generating the message
                        $message->render($this->template, $source);

                        // now we have to send out the message
                        if ($this->send($message)) $result++;                
                }

            // now we need to make sure something got sent
            if ($result == 0 && !$this->isDebug()) 
                throw new \RuntimeException("No message was sent.", 500);

            // now continue with we 
            return $inputs[self::FLD_OUTPUT];
        }

        /**
         * sending out the message to outside
         * @param Message $message the message to send
         * @return bool
         */
        protected abstract function send(Message $message) : bool;
    }