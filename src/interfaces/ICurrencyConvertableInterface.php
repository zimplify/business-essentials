<?php
    namespace Zimplify\Starter\Interfaces;

    /**
     * An interface to enable unit to activate the currency convertor
     * @package Zimplify\Starter (code 9)
     * @type interface (code 7)
     * @file ICurrencyConvertableInterface (codoe 01)
     */
    interface ICurrencyConvertableInterface {

        const PDR_CURRENCY_CONVERT = "starter::currency-convertor";
        
    }