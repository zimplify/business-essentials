<?php
    namespace Zimplify\Starter;
    use Zimplify\Core\Application;
    use Zimplify\Core\Message;
    use Zimplify\Core\Services\ClassUtils;
    use \RuntimeException;

    /**
     * the Ping object represent a in-app message that can be retrieved via websocket
     * @package Zimplify\Starter (code 9)
     * @type instance (code 1)
     * @file Email (code 11)
     */    
    class Notification extends Message {

        const CLS_AGENT = "Zimplify\\Core\\Agent";
        const DEF_EVERYBODY = "all";
        const SRF_ID = "id";
        
        /**
         * check whether the receipient is on system
         * @param string $receipient the ID of the user to receive the message
         * @return bool
         */
        protected function validate(string $receipient) : bool {
            $result = false;

            if ($receipient != self::DEF_EVERYBODY) {
                $person = Application::search([self::SRF_ID => $receipient]);

                // make sure the ID is a user object on system
                if (count($person) > 0) 
                    $result = ClassUtils::is($person[0], self::CLS_AGENT);                
            } else 
                $result = true;

            return $result;
            
        }
    }