<?php
    namespace Zimplify\Starter;
    use Zimplify\Starter\Staff;

    /**
     * the Administrator class is for us to easily define the application administrator into action
     * @package Zimplify\Starter (code 9)
     * @type instance (code 1)
     * @file Administrator (code 03)
     */
    final class Administrator extends Staff {
    }

    