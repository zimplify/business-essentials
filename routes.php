<?php
    
    // GROUP: the details over an account
    $this->group("/account", function () {

        // GROUP: actions for notifications for an account
        $this->group("/notifications", function () {

            //GET: get all notifications for the account
            $this->get("[/]", NotificationController::class.":fetch");

        })->add(new ValidateUserIdentity());

        // GROUP: actions for pings for the account
        $this->group("/pings", function () {

            // GROUP: the actions for a specific message
            $this->group("/{message}", function () {

                // DELETE: deleting for a specific message
                $this->delete("[/]", PingController::class.":delete");

                // GET: fetch data over a single message
                $this->get("[/]", PingController::class.":read");

            });
            

            // GET: get all pings assoicate with this account
            $this->get("[/]", PingController::class.":class");

            // POST: creating a new ping and send out
            $this->post("[/]", PingController::class.":create");

        })->add(new ValidateUserIdentity());

        // PUT: update the account credentials
        $this->put("[/]", AgentController::class.":reset")->add(new ValidateUserIdentity());
    });